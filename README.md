# OfficeHelper

> 方便个人使用的一个小程序,使用于WIn7和Win10,依赖于Net4.0

## 安装文件地址

[下载地址](http://data.imchenchao.com/files/soft/OfficeHelper/OfficeHelper.exe)

## 主要功能

* 可以设置开机自动启动,以及快捷键呼出的功能
* 打开软件显示托盘图标
* 主题更换功能
* 快速启动功能,需要将应用程序、快捷方式、文件、目录使用鼠标拖动到程序中
* 系统小工具功能:
  * 计算器
  * 记事本
  * 系统自带的截图工具
  * 画图
  * 控制面板
  * 命令行
  * 关闭显示器
  * 关机
* 增加翻译功能使用百度翻译API

![2017123112015](http://data.imchenchao.com/blog/images/2017123112015.png)

![2017123112040](http://data.imchenchao.com/blog/images/2017123112040.png)

![2017123112055](http://data.imchenchao.com/blog/images/2017123112055.png)

![2017123112112](http://data.imchenchao.com/blog/images/2017123112112.png)

## 引用的类库

* [CC.WPFTools](https://gitee.com/chenhome/WPFTOOLS)
* [MvvmLight](http://www.galasoft.ch/mvvm)
* [Hardcodet.NotifyIcon.Wpf](http://www.hardcodet.net/wpf-notifyicon)