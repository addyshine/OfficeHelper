﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace OfficeHelper.Model
{
    /// <summary>
    /// 百度翻译请求对象
    /// </summary>
    public class TranslateBaiduRequestObj
    {
        /// <summary>
        /// 请求翻译query
        /// </summary>
        public string q { get; set; }
        /// <summary>
        /// 翻译源语言
        /// </summary>
        public string from { get; set; } = "auto";
        /// <summary>
        /// 译文语言
        /// </summary>
        public string to { get; set; } = "zh";
        /// <summary>
        /// APP ID
        /// </summary>
        public long appid { get; set; }
        /// <summary>
        /// 随机数
        /// </summary>
        public int salt { get; set; }
        /// <summary>
        /// appid+q+salt+密钥 的MD5值
        /// </summary>
        public string sign { get; set; }
        /// <summary>
        /// 密钥
        /// </summary>
        public string key { get; set; }

        /// <summary>
        /// 获取请求字符串
        /// </summary>
        /// <returns></returns>
        public string GetRequestStr()
        {
            
            Random rd = new Random();
            //随机说生成
            salt = rd.Next();
            //计算签名
            string tmp = appid + q + salt.ToString() + key;
            //Console.WriteLine(tmpUtf8);
            sign = GetMD5WithString(tmp);

            string requestStr = $"http://api.fanyi.baidu.com/api/trans/vip/translate?q={q}&from={from}&to={to}&appid={appid}&salt={salt}&sign={sign}";

            return requestStr;
        }

        /// <summary>
        /// 获取请求字符串
        /// </summary>
        /// <returns></returns>
        public string GetRequestStrByChenChaoAPI()
        {
            return $"https://api.imchenchao.com/api/trans/?q={q}&from={from}&to={to}";
        }

        static public string GetMD5WithString(string input)
        {
            // Use input string to calculate MD5 hash
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.UTF8.GetBytes(input);
            byte[] hashBytes = md5.ComputeHash(inputBytes);

            // Convert the byte array to hexadecimal string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++)
            {
                sb.Append(hashBytes[i].ToString("x2"));
                // To force the hex string to lower-case letters instead of
                // upper-case, use he following line instead:
                // sb.Append(hashBytes[i].ToString("x2")); 
            }
            return sb.ToString();
        }

    }

    /// <summary>
    /// 百度翻译结果对象
    /// </summary>
    public class TranslateBaiduResultObj
    {
        public string from { get; set; }
        public string to { get; set; }
        public List<TranslateBaiduResult> trans_result { get; set; }
    }

    public class TranslateBaiduResult
    {
        public string src { get; set; }
        public string dst { get; set; }
    }

    #region json实体类

    public class TranslateMyAPITObj
    {
        /// <summary>
        /// 
        /// </summary>
        public bool success { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public TranslateBaiduResultObj result { get; set; }
    }
    #endregion

    /// <summary>
    /// 百度翻译语言列表
    /// </summary>
    public class TranslateBaiduLangue
    {

        public TranslateBaiduLangue()
        {

        }

        public TranslateBaiduLangue(string name,string value)
        {
            Name = name;
            Value = value;
        }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 语言值
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// 获取原语言
        /// </summary>
        /// <returns></returns>
        public static List<TranslateBaiduLangue> GetSrcLangues()
        {
            var srcLangues = new List<TranslateBaiduLangue>()
            {
                new TranslateBaiduLangue("自动检测","auto"),
                new TranslateBaiduLangue("中文","zh"),
                new TranslateBaiduLangue("英语","en"),
                new TranslateBaiduLangue("粤语","yue"),
                new TranslateBaiduLangue("文言文","wyw"),
                new TranslateBaiduLangue("日语","jp"),
                new TranslateBaiduLangue("韩语","kor"),
                new TranslateBaiduLangue("法语","fra"),
                new TranslateBaiduLangue("西班牙语","spa"),
                new TranslateBaiduLangue("泰语","th"),
                new TranslateBaiduLangue("阿拉伯语","ara"),
                new TranslateBaiduLangue("俄语","ru"),
                new TranslateBaiduLangue("葡萄牙语","pt"),
                new TranslateBaiduLangue("德语","de"),
                new TranslateBaiduLangue("意大利语","it"),
                new TranslateBaiduLangue("希腊语","el"),
                new TranslateBaiduLangue("荷兰语","nl"),
                new TranslateBaiduLangue("波兰语","pl"),
                new TranslateBaiduLangue("保加利亚语","bul"),
                new TranslateBaiduLangue("爱沙尼亚语","est"),
                new TranslateBaiduLangue("丹麦语","dan"),
                new TranslateBaiduLangue("芬兰语","fin"),
                new TranslateBaiduLangue("捷克语","cs"),
                new TranslateBaiduLangue("罗马尼亚语","rom"),
                new TranslateBaiduLangue("斯洛文尼亚语","slo"),
                new TranslateBaiduLangue("瑞典语","swe"),
                new TranslateBaiduLangue("匈牙利语","hu"),
                new TranslateBaiduLangue("繁体中文","cht"),
                new TranslateBaiduLangue("越南语","vie"),
            };

            return srcLangues;
        }

        /// <summary>
        /// 获取目标语言列表
        /// </summary>
        /// <returns></returns>
        public static List<TranslateBaiduLangue> GetDstLangues()
        {
            var srcLangues = new List<TranslateBaiduLangue>()
            {
                new TranslateBaiduLangue("中文","zh"),
                new TranslateBaiduLangue("英语","en"),
                new TranslateBaiduLangue("粤语","yue"),
                new TranslateBaiduLangue("文言文","wyw"),
                new TranslateBaiduLangue("日语","jp"),
                new TranslateBaiduLangue("韩语","kor"),
                new TranslateBaiduLangue("法语","fra"),
                new TranslateBaiduLangue("西班牙语","spa"),
                new TranslateBaiduLangue("泰语","th"),
                new TranslateBaiduLangue("阿拉伯语","ara"),
                new TranslateBaiduLangue("俄语","ru"),
                new TranslateBaiduLangue("葡萄牙语","pt"),
                new TranslateBaiduLangue("德语","de"),
                new TranslateBaiduLangue("意大利语","it"),
                new TranslateBaiduLangue("希腊语","el"),
                new TranslateBaiduLangue("荷兰语","nl"),
                new TranslateBaiduLangue("波兰语","pl"),
                new TranslateBaiduLangue("保加利亚语","bul"),
                new TranslateBaiduLangue("爱沙尼亚语","est"),
                new TranslateBaiduLangue("丹麦语","dan"),
                new TranslateBaiduLangue("芬兰语","fin"),
                new TranslateBaiduLangue("捷克语","cs"),
                new TranslateBaiduLangue("罗马尼亚语","rom"),
                new TranslateBaiduLangue("斯洛文尼亚语","slo"),
                new TranslateBaiduLangue("瑞典语","swe"),
                new TranslateBaiduLangue("匈牙利语","hu"),
                new TranslateBaiduLangue("繁体中文","cht"),
                new TranslateBaiduLangue("越南语","vie"),
            };

            return srcLangues;
        }
    }
}
