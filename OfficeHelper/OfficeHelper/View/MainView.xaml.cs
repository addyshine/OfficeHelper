﻿using CommonServiceLocator;
using GalaSoft.MvvmLight.Views;
using OfficeHelper.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OfficeHelper.View
{
    /// <summary>
    /// MainView.xaml 的交互逻辑
    /// </summary>
    public partial class MainView : UserControl
    {
        public MainView()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            //打开上次打开的界面
            if (!String.IsNullOrWhiteSpace(MySetting.LastOpenView))
            {
                NavigateTo(MySetting.LastOpenView);
            }
            else
            {
                NavigateTo("FastRun");
            }
        }

        /// <summary>
        /// 导航到页面
        /// </summary>
        /// <param name="Page"></param>
        public void NavigateTo(string Page)
        {
            try
            {
                var navigationService = ServiceLocator.Current.GetInstance<INavigationService>();
                navigationService.NavigateTo(Page);
                if (!string.IsNullOrWhiteSpace(Page))
                {
                    MySetting.LastOpenView = Page;
                }
            }
            catch (Exception ex)
            {
                //ShowMessageBox(ex.ToString());
            }

        }

        private SettingsModel MySetting = SettingsModel.Default;

        
    }
}
