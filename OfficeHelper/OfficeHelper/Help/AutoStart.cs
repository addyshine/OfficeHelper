﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace OfficeHelper.Help
{
    /// <summary>
    /// 自动启动类  Model设置启动模式 0 默认
    /// </summary>
    public class AutoStart
    {
        /// <summary>
        /// 设置模式 0 启动文件夹启动 1 注册表启动
        /// </summary>
        public static int Model = 0;

        /// <summary>
        /// 设置自动启动
        /// </summary>
        public static void SetAutoStart()
        {
            if (Model==0)
            {
                StratByStartupFolder();
            }
            else
            {
                StartByRegistry();
            }
        }
        /// <summary>
        /// 是否设置自动启动
        /// </summary>
        /// <returns></returns>
        public static bool IsAutoStartEnabled()
        {
            if (Model == 0)
            {
               return  GetIsAutoByStartupFolder();
            }
            else
            {
                return GetIsAutoByRegistry();
            }
        }
        /// <summary>
        /// 取消自动启动
        /// </summary>
        public static void UnSetAutoStart()
        {
            if (Model == 0)
            {
                StopByStartupFolder();
            }
            else
            {
                StopByRegistry(); 
            }
        }


        #region 启动文件夹启动方式
      

        /// <summary>
        /// 路径启动 获取当前登录用户的 开始 文件夹位置 win10 没有权限获取全局开始文件夹位置
        /// </summary>
        private static string StartupFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.Startup);
        /// <summary>
        /// 通过在启动文件夹复制快捷方式启动
        /// </summary>
        /// <returns></returns>
        private static bool StratByStartupFolder()
        {
            try
            {
                // 获取全局 开始 文件夹位置
                //string Folder =  Environment.GetFolderPath(Environment.SpecialFolder.CommonStartup);
                // 获取当前登录用户的 开始 文件夹位置 win10 没有权限获取全局开始文件夹位置
                //string Folder =  Environment.GetFolderPath(Environment.SpecialFolder.Startup);
                var exeInfo = System.Diagnostics.FileVersionInfo.GetVersionInfo(ExecutablePath);
                Shortcut.CreateShortcut(StartupFolderPath, exeInfo.ProductName, ExecutablePath);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 通过在启动文件夹复制快捷方式启动
        /// </summary>
        /// <returns></returns>
        private static bool StopByStartupFolder()
        {
            try
            {
                // 获取全局 开始 文件夹位置
                //string Folder = Environment.GetFolderPath(Environment.SpecialFolder.CommonStartup);
                // 获取当前登录用户的 开始 文件夹位置
                //string Folder =  Environment.GetFolderPath(Environment.SpecialFolder.Startup);
                var exeInfo = System.Diagnostics.FileVersionInfo.GetVersionInfo(ExecutablePath);
                string shortcutPath = Path.Combine(StartupFolderPath, string.Format("{0}.lnk", exeInfo.ProductName));
                if (File.Exists(shortcutPath))
                {
                    //删除快捷方式
                    File.Delete(shortcutPath);
                }
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 通过注册表设置是否自动启动
        /// </summary>
        /// <returns></returns>
        private static bool GetIsAutoByStartupFolder()
        {
            try
            {
                var exeInfo = System.Diagnostics.FileVersionInfo.GetVersionInfo(ExecutablePath);
                string shortcutPath = Path.Combine(StartupFolderPath, string.Format("{0}.lnk", exeInfo.ProductName));
                return File.Exists(shortcutPath);
            }
            catch (Exception)
            {
                throw;
            }
        } 
        #endregion

        #region 注册表操作方式 可能存在不是管理员 没有权限的问题

        //获取当前应用程序的exe路径
        private static string ExecutablePath = System.Windows.Forms.Application.ExecutablePath;
        private const string RunLocation = @"Software\Microsoft\Windows\CurrentVersion\Run";//自动启动注册表路径
        /// <summary>
        /// 通过注册表 设置当前程序开机自动运行
        /// </summary>
        private static bool StartByRegistry()
        {
            try
            {
                var exeInfo = System.Diagnostics.FileVersionInfo.GetVersionInfo(ExecutablePath);
                RegistryProxy.SetKey(RunLocation, exeInfo.ProductName, ExecutablePath);
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.ToString());
                return false;
            }
        }

        /// <summary>
        /// 通过注册表 停止当前程序开机自动运行
        /// </summary>
        private static bool StopByRegistry()
        {
            try
            {
                var exeInfo = System.Diagnostics.FileVersionInfo.GetVersionInfo(ExecutablePath);
                RegistryProxy.DeleteKey(RunLocation, exeInfo.ProductName);
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.ToString());
                return false;
            }
        }

        /// <summary>
        /// 通过注册表设置是否自动启动
        /// </summary>
        /// <returns></returns>
        private static bool GetIsAutoByRegistry()
        {
            try
            {
                var exeInfo = System.Diagnostics.FileVersionInfo.GetVersionInfo(ExecutablePath);
                string value = RegistryProxy.GetKey<string>(RunLocation, exeInfo.ProductName);
                if (value == null)
                    return false;
                return string.Equals(value, ExecutablePath, StringComparison.OrdinalIgnoreCase);
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.ToString());
                return false;
            }

        } 
        #endregion
    }
}
