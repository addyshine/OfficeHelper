﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace OfficeHelper.Help
{
    /// <summary>
    /// Exe相关操作
    /// </summary>
    public static  class ExeHelp
    {
        /// <summary>
        /// Exe路径
        /// </summary>
        public static string ExePath = "";
        /// <summary>
        /// 下载路径源 默认路径与 
        /// </summary>
        public static string HttpSource = @"http://data.imchenchao.com/officehelp/exe/";

        /// <summary>
        /// 下载文件到本地路径
        /// </summary>
        /// <param name="httpSource"></param>
        /// <param name="localFileName"></param>
        public static void DownHttp(string httpSource,string localFileName)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(httpSource);
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                Stream responseStream = response.GetResponseStream();
                Stream stream = new FileStream(localFileName, FileMode.Create);
                byte[] bArr = new byte[1024];
                int size = responseStream.Read(bArr, 0, bArr.Length);
                while (size > 0)
                {
                    stream.Write(bArr, 0, size);
                    size = responseStream.Read(bArr, 0, bArr.Length);
                }
                stream.Close();
                responseStream.Close();
            }
            catch (Exception)
            {

                //throw;
            }
            
        }
        /// <summary>
        /// 更新exe文件
        /// </summary>
        public static void UpdateExeFile()
        {
            //下载nircmd.exe
            string filePath = ExePath + "nircmd.exe";
            //如果文件不存在则下载到路径
            if (!File.Exists(filePath))
            {
                DownHttp(HttpSource+ "nircmd.exe", filePath);
            }
        }

        /// <summary>
        /// 直接执行Exe程序
        /// </summary>
        /// <param name="FilePath">exe路径</param>
        private static void Execute(string FilePath)
        {
            try
            {
                System.Diagnostics.ProcessStartInfo Info = new System.Diagnostics.ProcessStartInfo()
                {
                    FileName = FilePath//"calc.exe"为计算器，"notepad.exe"为记事本
                };
                System.Diagnostics.Process Proc = System.Diagnostics.Process.Start(Info);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// ExePath带参数执行Exe程序 
        /// </summary>
        /// <param name="FilePath">exe全路径</param>
        /// <param name="Arguments">exe参数</param>
        private static void ExecuteWithExePath(string FilePath,string Arguments)
        {
            try
            {
                System.Diagnostics.ProcessStartInfo Info = new System.Diagnostics.ProcessStartInfo()
                {
                    FileName =  FilePath,
                    Arguments = Arguments
                };
                System.Diagnostics.Process Proc = System.Diagnostics.Process.Start(Info);
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// 以管理员身份运行程序
        /// </summary>
        /// <param name="FileName"></param>
        /// <param name="Arguments">参数</param>
        public static void RunWithAdministrator(string FileName, string Arguments)
        {
            /** 
            * 当前用户是管理员的时候，直接启动应用程序 
            * 如果不是管理员，则使用启动对象启动程序，以确保使用管理员身份运行 
            */

            //获得当前登录的Windows用户标示 
            System.Security.Principal.WindowsIdentity identity = System.Security.Principal.WindowsIdentity.GetCurrent();
            //创建Windows用户主题 
            //Application.EnableVisualStyles();

            System.Security.Principal.WindowsPrincipal principal = new System.Security.Principal.WindowsPrincipal(identity);
            //判断当前登录用户是否为管理员 
            if (principal.IsInRole(System.Security.Principal.WindowsBuiltInRole.Administrator))
            {
                //如果是管理员，则直接运行 
                ExecuteWithExePath(FileName, Arguments);
            }
            else
            {
                //创建启动对象 
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                startInfo.FileName = FileName;
                if (Arguments != null && Arguments.Length > 0)
                {
                    startInfo.Arguments = Arguments;
                }
                //设置启动动作,确保以管理员身份运行 
                startInfo.Verb = "runas";
                //如果不是管理员，则启动UAC 
                System.Diagnostics.Process.Start(startInfo);
            }
        }

        /// <summary>
        /// 调用计算器
        /// </summary>
        public  static void Calc()
        {
            Execute("calc.exe");
        }


        /// <summary>
        /// 调用记事本
        /// </summary>
        public static void NotePad()
        {
            Execute("notepad.exe");
        }


        /// <summary>
        /// 调用截图工具
        /// </summary>
        public static void SnippingTool()
        {
            Execute("snippingtool.exe");
        }

        /// <summary>
        /// 打开控制面板
        /// </summary>
        public static void Control()
        {
            Execute("control.exe");
        }

        /// <summary>
        /// 打开画图板
        /// </summary>
        public static void Paint()
        {
            Execute("mspaint.exe");
        }

        /// <summary>
        /// 打开命令行
        /// </summary>
        public static void CMD()
        {
            Execute("cmd.exe");
        }

        /// <summary>
        /// 打开服务管理
        /// </summary>
        public static void Services()
        {
            Execute("services.msc");
        }



        /// <summary>
        /// 关闭显示器 依赖nircmd.exe
        /// </summary>
        public static void MonitorOff()
        {
            ExecuteWithExePath("nircmd.exe", "monitor off");
        }

        /// <summary>
        /// 关闭电脑 依赖nircmd.exe
        /// </summary>
        public static void PowerOff()
        {
            ExecuteWithExePath("nircmd.exe", "qboxcom \"立即关闭电脑? \" \"关机\" exitwin poweroff");
        }


       
    }
}
