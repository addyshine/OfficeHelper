using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Collections.ObjectModel;
using System.Threading;
using System.Windows.Threading;
using WPFTools.Dialogs;
using OfficeHelper.Help;

namespace OfficeHelper
{
    /// <summary>
    /// 单实例启动
    /// </summary>
    public class Startup
    {

        [STAThread]
        public static void Main(string[] args)
        {
           // Help.LogHelp.Instance.IsShowConsole = true;
           // Help.LogHelp.Instance.IsShowMessageBox = true;
          //  Help.LogHelp.Instance.Info("Welcome");
            SingleInstanceApplicationWrapper wrapper = new SingleInstanceApplicationWrapper();
            wrapper.Run(args);

        }
    }

    public class SingleInstanceApplicationWrapper :
        Microsoft.VisualBasic.ApplicationServices.WindowsFormsApplicationBase
    {
        public SingleInstanceApplicationWrapper()
        {
            // Enable single-instance mode.
            this.IsSingleInstance = true;
        }

        // Create the WPF application class.
        private OfficeHelper.App app;

        protected override bool OnStartup(
            Microsoft.VisualBasic.ApplicationServices.StartupEventArgs e)
        {
            app = new OfficeHelper.App();
            app.InitializeComponent();
            app.Run();
            return false;
        }


        /// <summary>
        /// 启动第二个实例时 激活已经打开的实例
        /// </summary>
        /// <param name="e"></param>
        protected override void OnStartupNextInstance(
            Microsoft.VisualBasic.ApplicationServices.StartupNextInstanceEventArgs e)
        {
            base.OnStartupNextInstance(e);
            //显示主窗体
            if (Application.Current.MainWindow.WindowState == WindowState.Minimized)
            {
                Application.Current.MainWindow.WindowState = WindowState.Normal;
            }
            Application.Current.MainWindow.Visibility = Visibility.Visible;
            Application.Current.MainWindow.Focus();
        }

        //public  partial class OfficeHelper.App : Application
        //{
        //    //protected override void OnStartup(System.Windows.StartupEventArgs e)
        //    //{
        //    //    base.OnStartup(e);

        //    //    //// Create and show the application's main window
        //    //    ////MainWindow window = new MainWindow();
        //    //    //OfficeHelper.App a = new App();
        //    //    //a.Run();
        //    //}

        //    public void Activate()
        //    {
        //        // Reactivate application's main window
        //        this.MainWindow.Show();
        //        this.MainWindow.Activate();
        //    }
        //}


        
    }


}
