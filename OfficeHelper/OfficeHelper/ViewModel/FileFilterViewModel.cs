using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using OfficeHelper.Help;
using System;
using System.Collections.Generic;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.Windows;
using CommonServiceLocator;
using Microsoft.WindowsAPICodePack.Dialogs;
using System.IO;
using System.Linq;
using System.Security;
using System.Text.RegularExpressions;

namespace OfficeHelper.ViewModel
{
    
    public class FileFilterViewModel : OfficeHelpViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public FileFilterViewModel()
        {
            SelectDirCommand = new RelayCommand(SelectDirMethod);
            LoadDirCommand = new RelayCommand(LoadDirMethod);
            SelectDir = MySetting.FileFilterSelectedDir;
            FileOpenCommand = new RelayCommand(FileOpenMethod, CanSelectRecord);
            FileOpenDirCommand = new RelayCommand(FileOpenDirMethod, CanSelectRecord);
            RegexMatchCommand = new RelayCommand(RegexMatchMethod);
        }

        #region 字段
        private string _SelectDir;
        /// <summary>
        /// 选择的目录
        /// </summary>
        public string SelectDir
        {
            get { return _SelectDir; }
            set
            {
                Set("SelectDir", ref _SelectDir, value);
            }
        }

        private ObservableCollection<DirectoryRecord> directoryRecords = new ObservableCollection<DirectoryRecord>();

        public ObservableCollection<DirectoryRecord> DirectoryRecords
        {
            get { return directoryRecords; }
            set
            {
                Set("DirectoryRecords", ref directoryRecords, value);
            }
        }

        private ObservableCollection<FileSystemRecord> fileSystemInfos;

        public ObservableCollection<FileSystemRecord> FileSystemInfos
        {
            get { return fileSystemInfos; }
            set
            {
                Set("FileSystemInfos", ref fileSystemInfos, value);
            }
        }

        private string _Info;

        public string Info
        {
            get { return _Info; }
            set
            {
                Set("Info", ref _Info, value);
            }
        }

        private string _FileCount;
        /// <summary>
        /// 文件计数
        /// </summary>
        public string FileCount
        {
            get { return _FileCount; }
            set
            {
                Set("FileCount", ref _FileCount, value);
            }
        }


        private FileSystemRecord _SelectRecord;

        /// <summary>
        /// 选定目录
        /// </summary>
        public FileSystemRecord SelectRecord
        {
            get { return _SelectRecord; }
            set
            {
                Set("SelectRecord", ref _SelectRecord, value);
            }
        }


        private string _RegexString= "^*$";

        /// <summary>
        /// 正则表达式字符串
        /// </summary>
        public string RegexString
        {
            get { return _RegexString; }
            set
            {
                Set("RegexString", ref _RegexString, value);
            }
        }

        #endregion

        #region 命令


        public ICommand RegexMatchCommand { get; set; }

        /// <summary>
        /// 正则匹配功能
        /// </summary>
        private void RegexMatchMethod()
        {
            try
            {
                if (m_FileSystemInfos != null && !string.IsNullOrEmpty(RegexString))
                {
                    Regex r = new Regex(RegexString.Trim());
                    var fileSystemRecords = new ObservableCollection<FileSystemRecord>();
                    foreach (var item in m_FileSystemInfos)
                    {
                        if (r.IsMatch(item.FullName))
                        {
                            FileSystemRecord record = new FileSystemRecord()
                            {
                                Info = item
                            };
                            fileSystemRecords.Add(record);
                        }
                    }
                    FileSystemInfos = fileSystemRecords;
                    FileCount = string.Format("文件总数:{0}{1}", Environment.NewLine, FileSystemInfos.Count);
                }
            }
            catch (Exception ex)
            {
                ShowMessageBox(ex.Message);
            }
        }


        public ICommand SelectDirCommand { get; set; }

        /// <summary>
        /// 选择文件夹
        /// </summary>
        private void SelectDirMethod()
        {
            var dialog = new CommonOpenFileDialog();
            dialog.IsFolderPicker = true;
            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                SelectDir = dialog.FileName;
                MySetting.FileFilterSelectedDir = SelectDir;
                MySetting.Save();
            };
        }

        public ICommand LoadDirCommand { get; set; }

        /// <summary>
        /// 加载目录
        /// </summary>
        private void LoadDirMethod()
        {
            if (System.IO.Directory.Exists(SelectDir))
            {
                Directory_load(SelectDir);
            }
            else
            {
                ShowMessageBox("目录:" + SelectDir + "不存在");
            }
        }

        public ICommand FileOpenDirCommand { get; set; }

        /// <summary>
        /// 打开文件所在目录
        /// </summary>
        private void FileOpenDirMethod()
        {
            var info = SelectRecord.Info;
            if (info.Exists)
            {
                //如果为目录
                if (info.Attributes == FileAttributes.Directory)
                {
                    System.Diagnostics.Process.Start(info.FullName);
                }
                else
                {
                    //表示为文件
                    FileInfo fi = info as FileInfo;
                    if (fi != null)
                    {
                        //System.Diagnostics.Process.Start("Explorer", "/select," + FilePath + "\\" + FileName);
                        System.Diagnostics.Process.Start("Explorer", "/select," + fi.FullName);
                    }
                }
            }
        }


        public ICommand FileOpenCommand { get; set; }

        /// <summary>
        /// 打开文件
        /// </summary>
        private void FileOpenMethod()
        {
            var info = SelectRecord.Info;
            if (info.Exists)
            {
                System.Diagnostics.Process.Start(info.FullName);
            }
        } 
        #endregion


        private bool CanSelectRecord()
        {
            if (SelectRecord!=null)
            {
                return true;
            }
            return false;

        }

        private List<FileSystemInfo> m_FileSystemInfos;

        //加载FolderPath目录下的文件夹及文件信息
        private void Directory_load(string dirPath)
        {
            try
            { 
                FileSystemInfos = null;

                m_FileSystemInfos = new List<FileSystemInfo>();
               //ObservableCollection<FileSystemInfo> list = new ObservableCollection<FileSystemInfo>();
                System.Threading.Tasks.Task.Factory.StartNew(new Action(
                    () => {
                        GetFileInfos(dirPath, m_FileSystemInfos, new Action<string>(x => {
                            Info = x;
                        }));
                        var fileSystemRecords = new ObservableCollection<FileSystemRecord>();
                        foreach (var item in m_FileSystemInfos)
                        {
                            FileSystemRecord record = new FileSystemRecord()
                            {
                                Info = item
                            };
                            fileSystemRecords.Add(record);
                        }
                        FileSystemInfos = fileSystemRecords;
                        FileCount = string.Format("文件总数:{0}{1}",Environment.NewLine, FileSystemInfos.Count);
                        //DirectoryRecords = directory;
                    }));
               
                return;
            }
            catch (SecurityException ex)
            {
                ShowMessageBox(ex.ToString());
            }
        }


        public void GetFileInfos(string dirPath, List<FileSystemInfo> list,Action<string> action)
        {
            try
            {
                DirectoryInfo dirInfo = new DirectoryInfo(dirPath);
                if (dirInfo.Exists)
                {
                    var fs = dirInfo.GetFileSystemInfos("*", SearchOption.TopDirectoryOnly);
                    if (action != null)
                    {
                        action(dirInfo.FullName); ;
                    }
                    if (fs != null)
                    {
                        foreach (var item in fs)
                        {
                            list.Add(item);
                            if (item.Attributes == FileAttributes.Directory)
                            { //如果是目录则继续执行
                                if (System.IO.Directory.Exists(item.FullName))
                                {
                                    GetFileInfos(item.FullName, list, action);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception )
            {

                //throw;
            }
            
        }
        
    }

    /// <summary>
    /// 自定义的文件记录
    /// </summary>
    public class FileSystemRecord
    {
        public FileSystemInfo Info { get; set; }
    }
    //利用IEnumerable接口实现对FileInfo及其属性GetDirectories的调用
    public class DirectoryRecord : ViewModelBase
    {
        public FileSystemInfo Info { get; set; }

        public IEnumerable<DirectoryRecord> Directories
        {
            get
            {
                //var dirInfo = Info as DirectoryInfo;
                if (Info is DirectoryInfo)
                {
                    var dirInfo = Info as DirectoryInfo;

                    List<FileSystemInfo> fileSystemInfos = new List<FileSystemInfo>();

                    var dirs = dirInfo.GetFileSystemInfos("*").Select(x => { return new DirectoryRecord { Info = x }; });
                    return dirs;
                }
                else
                {
                    return null;
                }

            }
        }
    }
}