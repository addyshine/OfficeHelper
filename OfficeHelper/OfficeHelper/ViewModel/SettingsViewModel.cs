using CommonServiceLocator;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using OfficeHelper.Help;
using OfficeHelper.Model;
using System;
using System.Collections.Generic;
using System.Windows.Input;

namespace OfficeHelper.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class SettingsViewModel : OfficeHelpViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public SettingsViewModel()
        {

            IsAutoRun = AutoStart.IsAutoStartEnabled();

            SaveSettingsCommand = new RelayCommand(SaveSettings);
            
            if (IsInDesignMode)
            {
               
            }
            else
            {

                // Code runs "for real"
            }
        }

        private SettingsModel _MySettings= SettingsModel.Default;

        public SettingsModel MySettings
        {
            get { return _MySettings; }
            set
            {
                Set("MySettings", ref _MySettings, value);
            }
        }


        /// <summary>
        /// 保存设置
        /// </summary>
        public ICommand SaveSettingsCommand { get; set; }

        private bool _IsAutoRun;

        public bool IsAutoRun
        {
            get { return _IsAutoRun; }
            set
            {
                Set("IsAutoRun", ref _IsAutoRun, value);
            }
        }


        private void SaveSettings()
        {
            //设置自动启动
            if (IsAutoRun)
            {
               AutoStart.SetAutoStart(); //设置自动运行
            }
            else
            {
                AutoStart.UnSetAutoStart(); //取消自动运行
            }

            //Model.SettingsModel.Default.HotKeys = HotKeys;
            Model.SettingsModel.Default.Save();
            //应用快捷键设置
            Model.HotKeySettingsManager.Instance.SaveGlobalHotKey();
            NavigateGoBack();//保存设置后返回
        }

    }
}