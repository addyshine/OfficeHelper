using CommonServiceLocator;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using OfficeHelper.Help;
using OfficeHelper.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace OfficeHelper.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class FastRunViewModel : OfficeHelpViewModelBase
    {

        private readonly LogHelp log = LogHelp.Instance;
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public FastRunViewModel()
        {
            TargetAddInit();

            ItemSelectionChangedCommand = new RelayCommand<SelectionChangedEventArgs>(ItemSelectionChanged);
            ItemMouseDoubleClickCommand = new RelayCommand(ItemMouseDoubleClick);
            ItemContextMenuCommand = new RelayCommand<string>(ItemContextMenu);
            TargetModelEditCommand = new RelayCommand<string>(TargetModelEdit);
            
            ////if (IsInDesignMode)
            ////{
            ////    // Code runs in Blend --> create design time data.
            ////}
            ////else
            ////{
            ////    // Code runs "for real"
            ////}
        }

        /// <summary>
        /// 复制文件 命令
        /// </summary>
        private void Copy()
        {
            DriveInfo[] allDrives = DriveInfo.GetDrives();

            //foreach (DriveInfo d in allDrives)
            //{
            //    Console.WriteLine("Drive {0}", d.Name);
            //    Console.WriteLine("  File type: {0}", d.DriveType);
            //}

            //Console.WriteLine("---------------------");


            List<string> RemoveablePath=new List<string>();
            foreach (DriveInfo d in allDrives)
            {
                //判断是不是U盘
                if (d.DriveType == DriveType.Removable)
                {
                    RemoveablePath.Add(d.Name);
                   // Console.WriteLine("Drive {0}", d.Name);
                }
            }

            if (RemoveablePath.Count>0)
            {
                //复制文件
                string sourceFloder = @"E:\Git\UrineSys\UrineSys\UrineSys\bin\x86\Release";
                CopyFolder(sourceFloder, RemoveablePath[0]);

            }

            //Console.ReadLine();
        }

        public static void CopyFolder(string strFromPath, string strToPath)
        {
            //如果源文件夹不存在，则创建
            if (!Directory.Exists(strFromPath))
            {
                Directory.CreateDirectory(strFromPath);
            }
            //取得要拷贝的文件夹名
            string strFolderName = strFromPath.Substring(strFromPath.LastIndexOf("\\") +
              1, strFromPath.Length - strFromPath.LastIndexOf("\\") - 1);
            //如果目标文件夹中没有源文件夹则在目标文件夹中创建源文件夹
            if (!Directory.Exists(strToPath + "\\" + strFolderName))
            {
                Directory.CreateDirectory(strToPath + "\\" + strFolderName);
            }
            //创建数组保存源文件夹下的文件名
            string[] strFiles = Directory.GetFiles(strFromPath);
            //循环拷贝文件
            for (int i = 0; i < strFiles.Length; i++)
            {
                //取得拷贝的文件名，只取文件名，地址截掉。
                string strFileName = strFiles[i].Substring(strFiles[i].LastIndexOf("\\") + 1, strFiles[i].Length - strFiles[i].LastIndexOf("\\") - 1);
                //开始拷贝文件,true表示覆盖同名文件
                File.Copy(strFiles[i], strToPath + "\\" + strFolderName + "\\" + strFileName, true);
            }
            //创建DirectoryInfo实例
            DirectoryInfo dirInfo = new DirectoryInfo(strFromPath);
            //取得源文件夹下的所有子文件夹名称
            DirectoryInfo[] ZiPath = dirInfo.GetDirectories();
            for (int j = 0; j < ZiPath.Length; j++)
            {
                //获取所有子文件夹名
                string strZiPath = strFromPath + "\\" + ZiPath[j].ToString();
                //把得到的子文件夹当成新的源文件夹，从头开始新一轮的拷贝
                CopyFolder(strZiPath, strToPath + "\\" + strFolderName);
            }
        }

        #region 增加项目


        /// <summary>
        /// 初始化
        /// </summary>
        private void TargetAddInit()
        {
            TargetAddSaveCommand = new RelayCommand(TargetAddSave);
            DropCommand = new RelayCommand<DragEventArgs>(Drop);
        }


        private Model.FastTargetModel _TargetADD = new Model.FastTargetModel();
        /// <summary>
        /// 增加的目标
        /// </summary>
        public Model.FastTargetModel TargetADD
        {
            get { return _TargetADD; }
            set
            {
                Set("TargetADD", ref _TargetADD, value);
            }
        }

       


        public ICommand TargetAddSaveCommand { get; set; }
        /// <summary>
        /// 目标增加保存
        /// </summary>
        private void TargetAddSave()
        {
            bool isSuccess = SettingsModel.Default.FastTargetAdd(TargetADD);
            if (isSuccess)
            {
                SettingsModel.Default.Save();
                TargetADD = new FastTargetModel();
            }
        }
        #endregion
        /// <summary>
        /// 拖动命令
        /// </summary>
        public ICommand DropCommand { get; set; }
        private void Drop(DragEventArgs e)
        {
            try
            {
                //LogHelp.Instance.Debug(e.ToString());
                //获取拖拽的文件
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

                if (files != null && files.Length > 0)
                {
                    foreach (var item in files)
                    {
                        LogHelp.Instance.Debug(item);
                        string filePath = item;
                        //判断是否是快捷方式
                        if (filePath.ToLower().Contains(".lnk"))
                        {
                            string targetPath = Shortcut.GetCreateShortcutTargetPath(filePath);
                            filePath = targetPath;
                            LogHelp.Instance.Debug(targetPath);
                        }

                        FastTargetModel model = new FastTargetModel();
                        //如果是文件
                        if (File.Exists(filePath))
                        {
                            FileInfo info = new FileInfo(filePath);
                            string fileNameWithoutExtension = System.IO.Path.GetFileNameWithoutExtension(filePath);// 没有扩展名的文件名 
                            model.Name = fileNameWithoutExtension;
                            model.Target = filePath;
                            //表示为exe程序
                            if (info.Extension.ToLower().Contains("exe"))
                            {
                                string iconPath = SettingsModel.Default.IconPath + DateTime.Now.ToString("yyyMMddhhmmssfff") + ".png";
                                var icon = SystemIcon.GetIcon48(filePath);
                                if (icon != null)
                                {
                                    model.ICON = iconPath;
                                    icon.ToBitmap().Save(model.ICON);
                                }
                            }
                            else
                            {
                                string iconPath = SettingsModel.Default.IconPath + info.Extension.Substring(1) + ".png";
                                model.ICON = iconPath;
                                //如果图标不存在则创建图标
                                if (!File.Exists(iconPath))
                                {
                                    var icon = SystemIcon.GetIcon48(filePath);
                                    if (icon != null)
                                    {
                                        icon.ToBitmap().Save(model.ICON);
                                    }
                                }
                                
                            }
                            
                        }
                        else  if (Directory.Exists(filePath))
                        {
                            //如果是目录
                            DirectoryInfo info = new DirectoryInfo(filePath);
                            LogHelp.Instance.Debug(info.Name);
                            model.Name = info.Name;
                            model.Target = filePath;
                            string iconPath = SettingsModel.Default.IconPath + "Dir.png";
                            model.ICON = iconPath;
                            //不存在获取并保存图标
                            if (!File.Exists(iconPath))
                            {
                                var icon = SystemIcon.GetDirectoryIcon(true);
                               //var icon = SystemIcon.GetIcon48(filePath);
                                if (icon != null)
                                {
                                   icon.ToBitmap().Save(model.ICON);
                                }
                            }
                        }
                        else
                        {
                            LogHelp.Instance.Error(filePath + "不是文件也不是目录");
                            return;
                        }
                        
                        //System.Drawing.Icon.ExtractAssociatedIcon(item).ToBitmap().Save(model.ICON);
                        bool isSuccess = SettingsModel.Default.FastTargetAdd(model);
                        if (isSuccess)
                        {
                            SettingsModel.Default.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelp.Instance.Error(ex.ToString());
            }

        }


        private Model.FastTargetModel _TargetSelectSingle;
        /// <summary>
        ///  选中的值
        /// </summary>
        public Model.FastTargetModel TargetSelectSingle
        {
            get { return _TargetSelectSingle; }
            set
            {
                Set("TargetSelectSingle", ref _TargetSelectSingle, value);
            }
        }

       
        private bool _IsDebug;
        /// <summary>
        /// 是否显示调试界面
        /// </summary>
        public bool IsDebug
        {
            get { return _IsDebug; }
            set
            {
                Set("IsDebug", ref _IsDebug, value);
                if (value==true)
                {
                    LogHelp.Instance.IsShowWPF = true;
                }
                else
                {
                    LogHelp.Instance.IsShowWPF = false;
                }
            }
        }

        /// <summary>
        /// 选择项目更改
        /// </summary>
        public ICommand ItemSelectionChangedCommand { get; set; }
        private void ItemSelectionChanged(SelectionChangedEventArgs e)
        {
            LogHelp.Instance.IsShowWPF = true;
            //LogHelp.Instance.Debug(TargetSelectSingle.Name);
        }

        /// <summary>
        /// 项目鼠标双击
        /// </summary>
        public ICommand ItemMouseDoubleClickCommand { get; set; }
        private void ItemMouseDoubleClick()
        {
            try
            {
                LogHelp.Instance.Debug(TargetSelectSingle.Name);
                if (TargetSelectSingle != null)
                {
                    string path = TargetSelectSingle.Target;
                    System.Diagnostics.Process.Start(path);
                }
            }
            catch (Exception ex)
            {

                WPFTools.Dialogs.MessageDialog.ShowDialog(ex.Message,"错误");
                //wpftooMessageBox.Show(ex.Message);
            }
            
        }
        /// <summary>
        /// 项目右击菜单
        /// </summary>
        public ICommand ItemContextMenuCommand { get; set; }
        /// <summary>
        /// 项目右击菜单
        /// </summary>
        /// <param name="opt">需要处理的参数</param>
        public void ItemContextMenu(string opt)
        {
            if (opt!=null && TargetSelectSingle != null)
            {
                switch (opt)
                {
                    case "add":
                        ItemContextMenuAdd();
                        break;
                    case "delete":
                        ItemContextMenuDelete();
                        break;
                    case "update":
                        ItemContextMenuUpdate();
                        break;
                    case "openFolder":
                        ItemContextMenuOpenFolder();
                        break;
                    case "runWithAdmin":
                        ItemContextMenuRunWithAdmin();
                        break;
                    default:
                        break;
                }

            }
        }
        /// <summary>
        /// 使用管理员身份运行
        /// </summary>
        private void ItemContextMenuRunWithAdmin()
        {
            if (TargetSelectSingle != null && TargetSelectSingle.Target != null)
            {
                string path = TargetSelectSingle.Target;
                ExeHelp.RunWithAdministrator(path, TargetSelectSingle.Param);
                ////路径直接打开
                //if (Directory.Exists(path))
                //{
                   
                //    System.Diagnostics.Process.Start(path);
                //}
                //else if (File.Exists(path))
                //{
                //    FileInfo info = new FileInfo(path);
                //    System.Diagnostics.Process.Start(info.DirectoryName);
                //}
            }
        }

        /// <summary>
        /// 打开所在文件夹
        /// </summary>
        private void ItemContextMenuOpenFolder()
        {
            if (TargetSelectSingle!=null && TargetSelectSingle.Target !=null)
            {
                string path = TargetSelectSingle.Target;
                //路径直接打开
                if (Directory.Exists(path))
                {
                    System.Diagnostics.Process.Start(path);
                }
                else if (File.Exists(path))
                {
                    FileInfo info = new FileInfo(path);
                    System.Diagnostics.Process.Start(info.DirectoryName);
                }
            }
        }

        /// <summary>
        /// 右键菜单更新
        /// </summary>
        private void ItemContextMenuUpdate()
        {
            TargetUpdate = TargetSelectSingle;
            NavigateTo("FastRunUpdate");
        }

        /// <summary>
        /// 右键菜单删除
        /// </summary>
        private void ItemContextMenuDelete()
        {
            try
            {
                if (Model.SettingsModel.Default.FastTargetList.Contains(TargetSelectSingle))
                {
                    var list = Model.SettingsModel.Default.FastTargetList;
                    list.Remove(TargetSelectSingle);
                    Model.SettingsModel.Default.Save();
                }
            }
            catch (Exception ex )
            {
                LogHelp.Instance.Error(ex.ToString());
            }
            
        }

        /// <summary>
        /// 增加项目
        /// </summary>
        private void ItemContextMenuAdd()
        {
            LogHelp.Instance.Debug(TargetSelectSingle.Name);
        }

        private Model.FastTargetModel _TargetUpdate;
        /// <summary>
        /// 更新的对象
        /// </summary>
        public Model.FastTargetModel TargetUpdate
        {
            get { return _TargetUpdate; }
            set
            {
                Set("TargetUpdate", ref _TargetUpdate, value);
            }
        }
        /// <summary>
        /// 目标编辑命令
        /// </summary>
        public ICommand TargetModelEditCommand { get; set; }
        private void TargetModelEdit(string info)
        {
            if (info!=null && info.Length >0)
            {
                switch (info)
                {
                    case "cancel":
                        NavigateTo("FastRun");
                        break;
                    case "update":
                        TargetUpdate.ValidateTargetInvalid();//验证状态
                        TargetSelectSingle = TargetUpdate;//更新选择
                        Model.SettingsModel.Default.Save();
                        NavigateTo("FastRun");
                        break;
                    default:
                        break;
                }
            }
        }

      
    }
}