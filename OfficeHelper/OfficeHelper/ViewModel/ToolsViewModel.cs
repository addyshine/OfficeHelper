﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace OfficeHelper.ViewModel
{
     public class ToolsViewModel: OfficeHelpViewModelBase
    {
        public ToolsViewModel()
        {
            ToolsRunCommand = new RelayCommand<string>(ToolsRun);
        }

        /// <summary>
        /// 运行命令
        /// </summary>
        /// <param name="msg"></param>
        private void ToolsRun(string msg)
        {
            switch (msg)
            {
                //throw new NotImplementedException();
                case "Calc":
                    Help.ExeHelp.Calc(); //调用计算器
                    break;
                case "NotePad":
                    Help.ExeHelp.NotePad(); //调用记事本
                    break;
                case "MonitorOff":
                    Help.ExeHelp.MonitorOff(); //关闭显示器
                    break;
                case "PowerOff":
                    Help.ExeHelp.PowerOff(); //关闭计算机
                    break;
                case "SnippingTool":
                    Help.ExeHelp.SnippingTool(); //截图工具
                    break;
                case "Control":
                    Help.ExeHelp.Control(); //控制面版
                    break;
                case "Paint":
                    Help.ExeHelp.Paint(); //控制画图工具
                    break;
                case "CMD":
                    Help.ExeHelp.CMD(); //命令行工具
                    break;
                case "Services":
                    Help.ExeHelp.Services(); //启动服务管理
                    break;

                default:
                    break;
            }
            
        }

        public ICommand ToolsRunCommand { get; set; }
    }
}
