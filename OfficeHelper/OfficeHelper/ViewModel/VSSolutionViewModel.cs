using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using OfficeHelper.Help;
using System;
using System.Collections.Generic;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.Windows;
using CommonServiceLocator;
using Microsoft.WindowsAPICodePack.Dialogs;
using System.IO;
using OfficeHelper.Model;
using Microsoft.CSharp;
using System.CodeDom.Compiler;

namespace OfficeHelper.ViewModel
{
    /// <summary>
    /// VS解决方案管理
    /// </summary>
    public class VSSolutionViewModel : OfficeHelpViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public VSSolutionViewModel()
        {
            SelectSolutionFile = MySetting.VSSolutionSelectSolutionFile;
            SearchText = MySetting.VSSolutionSearchText;
            SelectSolutionFileCommand = new RelayCommand(SelectSolutionFileMethod);
            LoadSolutionFileCommand = new RelayCommand(LoadSolutionFileMethod);
            SearchCommand = new RelayCommand(SearchMethod, new Func<bool>(() => m_DirInfoTree == null ? false : true));
            TreeContentMenuCommand = new RelayCommand<string>(TreeContentMenuMethod, new Func<string, bool>((x) => { return SelectNode == null ? false : true; }));
            TreeSelectItemChangeCommand = new RelayCommand<object>(TreeSelectItemChangeMethod);
            LoadCsCommand = new RelayCommand(LoadCsMethod);
        }


        public ICommand LoadCsCommand { get; set; }

        /// <summary>
        /// 加载CS文件
        /// </summary>
        private void LoadCsMethod()
        {
            if (SelectNode!=null)
            {
                var info = SelectNode.Data.Info as FileInfo;
                if (info != null && info.Exists && info.Extension == ".cs")
                {

                    //ShowMessageBox(code.LinePragma.ToString());
                }
                  
            }
        }




        #region 字段
        private string _INfo;

        public string Info
        {
            get { return _INfo; }
            set
            {
                Set("Info", ref _INfo, value);
            }
        }



        private string _SelectSolutionFile;
        /// <summary>
        /// 选择的目录
        /// </summary>
        public string SelectSolutionFile
        {
            get { return _SelectSolutionFile; }
            set
            {
                Set("SelectSolutionFile", ref _SelectSolutionFile, value);
            }
        }


        private List<DirectoryRecord> _DirectoryRecords;

        /// <summary>
        /// 目录记录
        /// </summary>
        public List<DirectoryRecord> DirectoryRecords
        {
            get { return _DirectoryRecords; }
            set
            {
                Set("DirectoryRecords", ref _DirectoryRecords, value);
            }
        }

        private string _SearchText;

        public string SearchText
        {
            get { return _SearchText; }
            set
            {
                Set("SearchText", ref _SearchText, value);
            }
        }

        private Tree<DirInfo> _SelectNode;

        /// <summary>
        /// 选中的节点
        /// </summary>
        public Tree<DirInfo> SelectNode
        {
            get { return _SelectNode; }
            set
            {
                Set("SelectNode", ref _SelectNode, value);
            }
        }


        #endregion

        #region 命令

        public ICommand TreeSelectItemChangeCommand { get; set; }

        /// <summary>
        /// 选中节点更改时
        /// </summary>
        private void TreeSelectItemChangeMethod(object param)
        {
            var info = param as Tree<DirInfo>;
            if (info != null)
            {
                SelectNode = info;
                Info = info.Data.Info.FullName;
            }
        }

        public ICommand TreeContentMenuCommand { get; set; }

        /// <summary>
        /// 树控件右键菜单
        /// </summary>
        private void TreeContentMenuMethod(string param)
        {
            if (SelectNode!=null)
            {
                if (param=="copyName")
                {
                    Clipboard.SetText(SelectNode.Data.Info.Name);
                }

                if (param == "copyPath")
                {
                    Clipboard.SetText(SelectNode.Data.Info.FullName);
                }

                //直接打开
                if (param == "openFile")
                #region 直接打开
                {
                    try
                    {
                        //如果是目录则直接打开
                        if (SelectNode.Data.Info.Attributes==FileAttributes.Directory)
                        {
                            System.Diagnostics.Process.Start(SelectNode.Data.Info.FullName);
                        }
                        else
                        {
                            string fileName = SelectNode.Data.Info.FullName;
                            if (File.Exists(fileName))
                            {
                                System.Diagnostics.Process.Start(fileName);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ShowMessageBox(ex.ToString());
                    }
                    
                }
                #endregion

                //打开所在文件夹
                if (param == "openDir")
                #region 打开所在文件夹
                {
                    try
                    {
                        var filesysInfo = SelectNode.Data.Info;
                        if (filesysInfo != null)
                        {
                            //如果为目录
                            if (filesysInfo.Attributes == FileAttributes.Directory)
                            {
                                System.Diagnostics.Process.Start(filesysInfo.FullName);
                            }
                            else
                            {
                                //表示为文件
                                FileInfo fi = new FileInfo(filesysInfo.FullName);
                                if (fi.Exists)
                                {
                                    //System.Diagnostics.Process.Start("Explorer", "/select," + FilePath + "\\" + FileName);
                                    System.Diagnostics.Process.Start("Explorer", "/select," + fi.FullName);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ShowMessageBox(ex.ToString());
                    }

                }
                #endregion

            }
        }

        public ICommand SearchCommand { get; set; }

        /// <summary>
        /// 查找命令
        /// </summary>
        private void SearchMethod()
        {
            SelectNode = null;
            if (!string.IsNullOrWhiteSpace(SearchText))
            {
                MySetting.VSSolutionSearchText = SearchText;

                Tree<DirInfo> newTree = new Tree<DirInfo>();
                findTree(newTree,m_DirInfoTree,SearchText);
                DelEmptyNode(newTree);

                DirectoryTree = newTree;
                //List<DirectoryRecord> result = new List<DirectoryRecord>();

                //findNode(m_directoryRecords, SearchText, result);
              
            }
            else
            {
                DirectoryTree = m_DirInfoTree;
            }
        }

        /// <summary>
        /// 查找匹配的节点
        /// </summary>
        /// <param name="newNode"></param>
        /// <param name="srcNode"></param>
        /// <param name="searchTxt"></param>
        private void findTree(Tree<DirInfo> newNode, Tree<DirInfo> srcNode,string searchTxt)
        {
            if (srcNode!=null)
            {
                if (srcNode.Nodes!=null)
                {
                    //遍历原始节点
                    foreach (var item in srcNode.Nodes)
                    {
                        Tree<DirInfo> node = new Tree<DirInfo>();
                        newNode.AddNode(node);
                        var data = item.Data;
                        if (data != null)
                        {
                            var info = data.Info;
                            if (info != null && info.Name.IndexOf(searchTxt) >= 0)
                            {
                                //复制当前节点
                                node.Data = data;
                                //node.Data.DisPlayName = "<Run Foreground=\"Red\">"+ node.Data.DisPlayName + "</Run>";
                                //复制当前节点的父级节点
                                if (item.Parent != null)
                                {
                                    //具有
                                    node.Parent.Data = item.Parent.Data;
                                }
                                //node.Parent.Data = new DirInfo() { Info=item.Parent.Data.Info};
                            }
                        }
                        findTree(node, item, searchTxt);
                    }
                }

            }
           
        }

        /// <summary>
        /// 删除空白节点
        /// </summary>
        private void DelEmptyNode(Tree<DirInfo> Node)
        {
            if (Node!=null)
            {
                //空集合
                List<Tree<DirInfo>> emptyNodes = new List<Tree<DirInfo>>();
                foreach (var item in Node.Nodes)
                {
                    //如果为空则记录
                    if (item.Data == null)
                    {
                        emptyNodes.Add(item);
                    }
                }
                //移除空节点
                foreach (var emptyNode in emptyNodes)
                {
                    Node.Nodes.Remove(emptyNode);
                }
                //递归删除空子节点
                foreach (var item in Node.Nodes)
                {
                    if (item.Nodes!=null&&item.Nodes.Count>0)
                    {
                        DelEmptyNode(item);
                    }
                }
                //
            }

           
           
        }


        private void findNode(List<DirectoryRecord> dirs, string strValue, List<DirectoryRecord> result)
        {

            foreach (var item in dirs)
            {

               string name =  item.Info.Name;
                if (name.IndexOf(strValue)>=0)
                {
                    result.Add(item);
                }

                if (item.Directories!=null)
                {
                    findNode(item.Directories, strValue, result);
                }
               
            }

            

            //if (tnParent.Info.FullName.IndexOf(strValue) > -1 && !string.IsNullOrEmpty(strValue))
            //{
            //    result.Add(tnParent);
            //}
            //foreach (var foo in tnParent.Directories)
            //{
            //    findNode(foo, strValue,result);
            //}
        }


        public ICommand SelectSolutionFileCommand { get; set; }

        /// <summary>
        /// 选择sln文件
        /// </summary>
        private void SelectSolutionFileMethod()
        {
            var dialog = new CommonOpenFileDialog();
            dialog.Filters.Add(new CommonFileDialogFilter("vs项目文件", "*.sln;*.csproj"));
            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                SelectSolutionFile = dialog.FileName;
                MySetting.VSSolutionSelectSolutionFile = SelectSolutionFile;
                MySetting.Save();
            };
        }

        public ICommand LoadSolutionFileCommand { get; set; }

        /// <summary>
        /// 加载目录
        /// </summary>
        private void LoadSolutionFileMethod()
        {
            if (System.IO.File.Exists(SelectSolutionFile))
            {
                SolutionFile_load(SelectSolutionFile);
            }
            else
            {
                ShowMessageBox("文件:" + SelectSolutionFile + "不存在");
            }
        }


        private Tree<DirInfo> _tree;

        public Tree<DirInfo> DirectoryTree
        {
            get { return _tree; }
            set
            {
                Set("DirectoryTree", ref _tree, value);
            }
        }


        Tree<DirInfo> m_DirInfoTree;
        /// <summary>
        /// 加载文件
        /// </summary>
        /// <param name="selectSolutionFile"></param>
        private void SolutionFile_load(string selectSolutionFile)
        {

            FileInfo fi = new FileInfo(selectSolutionFile);
            //获取文件所在目录
            string dirPath = fi.DirectoryName;
            //m_directoryRecords = new List<DirectoryRecord>();
            //GetFileInfos(dirPath, m_directoryRecords);

            //DirectoryRecords = m_directoryRecords;

            m_DirInfoTree = new Tree<DirInfo>();
            GetDirInfoTree(dirPath, m_DirInfoTree);
            DirectoryTree = m_DirInfoTree;
        }

        public ICommand FileOpenSolutionFileCommand { get; set; }

        #endregion

       public void GetDirInfoTree(string dirPath, Tree<DirInfo> tree)
        {
            try
            {
                DirectoryInfo dirInfo = new DirectoryInfo(dirPath);
                if (dirInfo.Exists)
                {
                    var dirs = dirInfo.GetDirectories("*", SearchOption.TopDirectoryOnly);
                    if (dirs != null)
                    {
                        foreach (var item in dirs)
                        {
                            var nodeTree = new Tree<DirInfo>
                            {
                                Data = new DirInfo() { Info = item, DisPlayName = item.Name }
                            };
                            tree.AddNode(nodeTree);
                            GetDirInfoTree(item.FullName, nodeTree);
                        }
                    }

                    var files = dirInfo.GetFiles("*", SearchOption.TopDirectoryOnly);
                    if (dirs != null)
                    {
                        foreach (var item in files)
                        {
                            var nodeTree = new Tree<DirInfo>
                            {
                                Data = new DirInfo() { Info = item, DisPlayName = item.Name }
                            };
                            tree.AddNode(nodeTree);//(new DirectoryRecord() { Info = item });
                        }
                    }
                }
            }
            catch (Exception )
            {

                //throw;
            }
        }


        public void GetFileInfos(string dirPath, List<DirectoryRecord> list, Action<string> action=null)
        {
            try
            {
                DirectoryInfo dirInfo = new DirectoryInfo(dirPath);
                if (dirInfo.Exists)
                {

                    //var fs = dirInfo.GetFileSystemInfos("*", SearchOption.TopDirectoryOnly);
                    if (action != null)
                    {
                        action(dirInfo.FullName); ;
                    }
                    var dirs = dirInfo.GetDirectories("*", SearchOption.TopDirectoryOnly);
                    if (dirs!=null)
                    {
                        foreach (var item in dirs)
                        {
                            var dirrecord = new DirectoryRecord() { Info = item };
                            dirrecord.Directories = new List<DirectoryRecord>();
                            GetFileInfos(item.FullName, dirrecord.Directories);
                            list.Add(dirrecord);
                        }
                    }

                    var files = dirInfo.GetFiles("*", SearchOption.TopDirectoryOnly);
                    if (dirs != null)
                    {
                        foreach (var item in files)
                        {
                            list.Add(new DirectoryRecord() { Info = item });
                        }
                    }

                    //if (fs!=null)
                    //{
                    //    foreach (var item in fs)
                    //    {
                    //        if (item.Attributes==FileAttributes.Directory)
                    //        {//为目录则继续执行
                    //            var dirrecord = new DirectoryRecord() { Info = item };

                    //            dirrecord.Directories = new List<DirectoryRecord>();
                    //            GetFileInfos(item.FullName, dirrecord.Directories);

                    //            list.Add(dirrecord);
                    //        }
                    //        else
                    //        {
                    //            list.Add(new DirectoryRecord() { Info=item});
                    //        }
                    //    }
                    //}
                }
            }
            catch (Exception )
            {

                //throw;
            }

        }

        //利用IEnumerable接口实现对FileInfo及其属性GetDirectories的调用
        public class DirectoryRecord
        {
            public FileSystemInfo Info { get; set; }

            public List<DirectoryRecord> Directories { get; set; }
        }


        public class DirInfo
        {
            public FileSystemInfo Info { get; set; }
            public string DisPlayName { get; set; }

        }
    }
}