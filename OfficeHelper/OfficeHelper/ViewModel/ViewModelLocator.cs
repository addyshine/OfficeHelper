/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:OfficeHelper"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using CommonServiceLocator;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using System;

namespace OfficeHelper.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            ////if (ViewModelBase.IsInDesignModeStatic)
            ////{
            ////    // Create design time view services and models
            ////    SimpleIoc.Default.Register<IDataService, DesignDataService>();
            ////}
            ////else
            ////{
            ////    // Create run time view services and models
            ////    SimpleIoc.Default.Register<IDataService, DataService>();
            ////}

            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<FastRunViewModel>();
            SimpleIoc.Default.Register<SettingsViewModel>();
            SimpleIoc.Default.Register<ToolsViewModel>();
            SimpleIoc.Default.Register<TranslateViewModel>();
            SimpleIoc.Default.Register<SQLGenerateViewModel>();
            SimpleIoc.Default.Register<FileFilterViewModel>();
            SimpleIoc.Default.Register<VSSolutionViewModel>();
            SimpleIoc.Default.Register<CodeGenerate.CodeGenerateIndexViewModel>();
            SimpleIoc.Default.Register<CodeGenerate.ReportGenerateConfigViewModel>();
            SimpleIoc.Default.Register<LongRuan.LRIndexViewModel>();


            //添加导航
            var navigationService = this.CreateNavigationService();
            SimpleIoc.Default.Register<INavigationService>(() => navigationService);
        }


        private INavigationService CreateNavigationService()
        {
            var navigationService = new NavigationService();
            navigationService.Configure("FastRun", new Uri("../View/FastRunView.xaml", UriKind.Relative));
            navigationService.Configure("FastRunUpdate", new Uri("../View/FastRunViewUpdate.xaml", UriKind.Relative));
            navigationService.Configure("Settings", new Uri("../View/SettingsView.xaml", UriKind.Relative));
            navigationService.Configure("Tools", new Uri("../View/ToolsView.xaml", UriKind.Relative));
            navigationService.Configure("Translate", new Uri("../View/TranslateView.xaml", UriKind.Relative));
            navigationService.Configure("SQLGenerate", new Uri("../View/SQLGenerateView.xaml", UriKind.Relative));
            navigationService.Configure("FileFilter", new Uri("../View/FileFilterView.xaml", UriKind.Relative));
            navigationService.Configure("VSSolution", new Uri("../View/VSSolutionView.xaml", UriKind.Relative));
            navigationService.Configure("CodeGenerateIndex", new Uri("../View/CodeGenerate/CodeGenerateIndexView.xaml", UriKind.Relative));
            navigationService.Configure("LRIndex", new Uri("../View/LongRuan/LRIndexView.xaml", UriKind.Relative));



            return navigationService;
        }

        public MainViewModel Main
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MainViewModel>();
            }
        }

        public FastRunViewModel FastRun
        {
            get
            {
                return ServiceLocator.Current.GetInstance<FastRunViewModel>();
            }
        }

        public SettingsViewModel Settings
        {
            get
            {
                return ServiceLocator.Current.GetInstance<SettingsViewModel>();
            }
        }

        public ToolsViewModel Tools
        {
            get
            {
                return ServiceLocator.Current.GetInstance<ToolsViewModel>();
            }
        }


        public TranslateViewModel Translate
        {
            get
            {
                return ServiceLocator.Current.GetInstance<TranslateViewModel>();
            }
        }

        /// <summary>
        /// SQL生成语句
        /// </summary>
        public SQLGenerateViewModel SQLGenerate
        {
            get
            {
                return ServiceLocator.Current.GetInstance<SQLGenerateViewModel>();
            }
        }

        // <summary>
        /// 文件过滤
        /// </summary>
        public FileFilterViewModel FileFilter
        {
            get
            {
                return ServiceLocator.Current.GetInstance<FileFilterViewModel>();
            }
        }

        // <summary>
        /// VS解决方案
        /// </summary>
        public VSSolutionViewModel VSSolution
        {
            get
            {
                return ServiceLocator.Current.GetInstance<VSSolutionViewModel>();
            }
        }


        /// <summary>
        /// 代码生成主页
        /// </summary>
        public CodeGenerate.CodeGenerateIndexViewModel CodeGenerateIndex
        {
            get
            {
                return ServiceLocator.Current.GetInstance<CodeGenerate.CodeGenerateIndexViewModel>();
            }
        }


        /// <summary>
        /// 报告生成主页
        /// </summary>
        public CodeGenerate.ReportGenerateConfigViewModel ReportGenerateConfig
        {
            get
            {
                return ServiceLocator.Current.GetInstance<CodeGenerate.ReportGenerateConfigViewModel>();
            }
        }


        /// <summary>
        /// 报告生成主页
        /// </summary>
        public LongRuan.LRIndexViewModel LRIndex
        {
            get
            {
                return ServiceLocator.Current.GetInstance<LongRuan.LRIndexViewModel>();
            }
        }


        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }
}