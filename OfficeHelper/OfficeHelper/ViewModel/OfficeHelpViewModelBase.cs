﻿using CommonServiceLocator;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using OfficeHelper.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace OfficeHelper.ViewModel
{

    /// 自定义的基础类
    /// </summary>
    public class OfficeHelpViewModelBase : ViewModelBase
    {
        public OfficeHelpViewModelBase()
        {
            NavigateToCommand = new RelayCommand<string>(NavigateTo);
            NavigateGoBackCommand = new RelayCommand(NavigateGoBack);

        }

        public ICommand NavigateToCommand { get; set; }

        /// <summary>
        /// 导航到页面
        /// </summary>
        /// <param name="Page"></param>
        public void NavigateTo(string Page)
        {
            try
            {
                var navigationService = ServiceLocator.Current.GetInstance<INavigationService>();
                navigationService.NavigateTo(Page);
                if (!string.IsNullOrWhiteSpace(Page))
                {
                    MySetting.LastOpenView = Page;
                }
            }
            catch (Exception ex)
            {
                ShowMessageBox(ex.ToString());
            }
           
        }

        public ICommand NavigateGoBackCommand { get; set; }
        /// <summary>
        /// 返回上一个页面
        /// </summary>
        public void NavigateGoBack()
        {
            ServiceLocator.Current.GetInstance<INavigationService>().GoBack();
        }


        private SettingsModel _mySetting = SettingsModel.Default;

        /// <summary>
        /// 设置属性
        /// </summary>
        public SettingsModel MySetting
        {
            get { return _mySetting; }
            set
            {
                Set("MySetting", ref _mySetting, value);
            }
        }



        /// <summary>
        /// 创建目录(目录不存在是)
        /// </summary>
        /// <param name="dirpath">路径</param>
        /// <returns></returns>
        protected bool CreateDir(string dirpath)
        {
            try
            {
                if (string.IsNullOrEmpty(dirpath))
                {
                    return false;
                }
                System.IO.DirectoryInfo directoryInfo = new System.IO.DirectoryInfo(dirpath);
                //不存在则新建
                if (!directoryInfo.Exists)
                {
                    directoryInfo.Create();

                }
                return true;
            }
            catch (Exception )
            {
                return false;
            }
        }

        protected   void ShowMessageBox(string text,string title=" " )
        {

            App.Current.Dispatcher.Invoke((Action)(() =>
            {
                WPFTools.Dialogs.MessageDialog.ShowDialog(text, title, System.Windows.MessageBoxButton.OK, App.Current.MainWindow, true);
            }));
            
        }
    }
}
