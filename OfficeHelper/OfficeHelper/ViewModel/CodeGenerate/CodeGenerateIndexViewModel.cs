﻿using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace OfficeHelper.ViewModel.CodeGenerate
{
    public class CodeGenerateIndexViewModel : OfficeHelpViewModelBase
    {
        public CodeGenerateIndexViewModel()
        {
            ConnAddCommand = new RelayCommand(ConnAddMethod);
        }



        public ICommand ConnAddCommand { get; set; }

        /// <summary>
        /// 数据链接增加
        /// </summary>
        private void ConnAddMethod()
        {
            View.CodeGenerate.ConnStrConfigView connStrConfigView = new View.CodeGenerate.ConnStrConfigView();
            connStrConfigView.ShowDialog();
        }

    }
}
